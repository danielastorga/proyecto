import 'package:app_homs_rentals/app/data/models/response/response_category_model.dart';
import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/data/providers/product_provider.dart';
import 'package:get/get.dart';

class ProductRepository {
  final _apiProvider = Get.find<ProductProvider>();

  Future<List<ResponseCategoryModel>> searchProductCategory({
    required int page,
    required String queryString,
  }) =>
      _apiProvider.searchProductCategory(
        page: page,
        queryString: queryString,
      );

  Future<List<ResponseProductByCategoryModel>> getAllProductsByCategory({
    required int page,
    required int categoryId,
  }) =>
      _apiProvider.getAllProductsByCategory(
        page: page,
        categoryId: categoryId,
      );

  Future<List<ResponseProductByCategoryModel>> getAllProductComplements({
    required int page,
    required String include,
  }) =>
      _apiProvider.getAllProductComplements(
        page: page,
        include: include,
      );

   Future<List<ResponseProductByCategoryModel>> getFeaturedProducts() => _apiProvider.getFeaturedProducts();
}
