import 'package:app_homs_rentals/app/data/providers/user_provider.dart';
import 'package:get/get.dart';

class UserRepository {
  final _apiProvider = Get.find<UserProvider>();

  Future<String> user_Register()=> _apiProvider.user_Register();
}