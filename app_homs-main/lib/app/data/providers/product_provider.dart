import 'package:app_homs_rentals/app/data/models/response/response_category_model.dart';
import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:dio/dio.dart';

class ProductProvider {
  final _dio = Dio();

  Future<List<ResponseCategoryModel>> searchProductCategory({
    required int page,
    required String queryString,
  }) async {
    final _response = await _dio.get(
      "https://homs1852.com/wp-json/wc/v3/products/categories",
      queryParameters: {
        "consumer_key": "ck_32ef1e230426e4933810402faab818e2fe794a4e",
        "consumer_secret": "cs_a8d8274aefe2c69cf6928d460d1729d5e9e2cabc",
        //"status": "publish",
        "search": queryString,
        "page": 1,
        "exclude": [102],
      },
    );
    //print(_response);
    return _response.data != null
        ? (_response.data as List)
            .map((item) => ResponseCategoryModel.fromJson(item))
            .toList()
        : [];
  }

  Future<List<ResponseProductByCategoryModel>> getAllProductsByCategory({
    required int page,
    required int categoryId,
  }) async {
    final _response = await _dio.get(
      "https://homs1852.com/wp-json/wc/v3/products",
      queryParameters: {
        "consumer_key": "ck_32ef1e230426e4933810402faab818e2fe794a4e",
        "consumer_secret": "cs_a8d8274aefe2c69cf6928d460d1729d5e9e2cabc",
        "status": "publish",
        "category": categoryId,
        "page": page,
        //"exclude": [102],
      },
    );

    return _response.data != null
        ? (_response.data as List)
            .map((item) => ResponseProductByCategoryModel.fromJson(item))
            .toList()
        : [];
  }

  Future<List<ResponseProductByCategoryModel>> getAllProductComplements({
    required int page,
    required String include,
  }) async {
    final _response = await _dio.get(
      "https://homs1852.com/wp-json/wc/v3/products",
      queryParameters: {
        "consumer_key": "ck_32ef1e230426e4933810402faab818e2fe794a4e",
        "consumer_secret": "cs_a8d8274aefe2c69cf6928d460d1729d5e9e2cabc",
        "status": "publish",
        "include": include,
      },
    );

    return _response.data != null
        ? (_response.data as List)
            .map((item) => ResponseProductByCategoryModel.fromJson(item))
            .toList()
        : [];
  }

   Future<List<ResponseProductByCategoryModel>> getFeaturedProducts() async {
    final dio = Dio();
    final response = await dio.get(
      "https://homs1852.com/wp-json/wc/v3/products",
      queryParameters: {
        "consumer_key": "ck_32ef1e230426e4933810402faab818e2fe794a4e",
        "consumer_secret": "cs_a8d8274aefe2c69cf6928d460d1729d5e9e2cabc",
        "featured": true,
        "per_page": 12
      },
      //  "https://reqres.in/api/users",
      // queryParameters: {
      //   "page":2
      //   }
    );

    //print(response.data);

    return (response.data as List)
        .map((json) => ResponseProductByCategoryModel.fromJson(json))
        .toList();
  }

}
