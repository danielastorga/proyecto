import 'package:flutter/widgets.dart';

class SectionModel {
  SectionModel({
    this.title = "",
    this.body = const SizedBox(),
    this.isExpanded = false,
  });

  late String title;
  late Widget body;
  late bool isExpanded;
}
