import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';

class CustomCardProductFeatured extends StatelessWidget {
  const CustomCardProductFeatured({
    Key? key,
    required this.productPath,
    required this.category,
    required this.productName,
    required this.fianza,
    required this.price,
    required this.isOnline,
  }) : super(key: key);

  final String productPath;
  final String category;
  final String productName;
  final String fianza;
  final String price;
  final bool isOnline;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(5.0),
          width: 200.0,
          //height: 120.0,
          margin: const EdgeInsets.only(left: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppValues.kRadiusSmall),
            border: Border.all(
              color: Color.fromARGB(255, 228, 229, 232),
            ),           
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Hero(tag: "$productPath",child: Image.network("$productPath")),
            Text("$category",maxLines: 1,overflow: TextOverflow.ellipsis,),
            Text("$productName", maxLines: 1,overflow: TextOverflow.ellipsis,),
            Text("Fianza", style: TextStyle(color: AppColors.kPrimary,),),
            Text(isOnline ? "$fianza" : "Segun presupuesto"),
            Text(isOnline ? "$price" : ""),
          ]),
        ),
        // Center(
        //   child: Image.network(
        //     "$productPath",
        //     width: 120.0,
        //   ),
        // ),
      ],
    );
  }
}
