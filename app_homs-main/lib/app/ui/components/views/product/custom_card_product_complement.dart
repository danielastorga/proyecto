import 'package:flutter/material.dart';

class CustomCardProductComplement extends StatelessWidget {
  const CustomCardProductComplement({
    Key? key,
    required this.productPath,
    required this.productName,
    required this.price,
  }) : super(key: key);

  final String productPath;
  final String productName;
  final String price;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Image.network(
            "$productPath",
            width: 120.0,
          ),
        ),
        Text("$productName"),
        Text("Tarifa"),
        Text("$price"),
      ],
    );
  }
}
