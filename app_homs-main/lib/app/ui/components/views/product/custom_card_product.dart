import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomCardProduct extends StatelessWidget {
  const CustomCardProduct({
    Key? key,
    required this.productPath,
    required this.category,
    required this.productName,
    required this.fianza,
    required this.price,
    required this.isOnline,
  }) : super(key: key);

  final String productPath;
  final String category;
  final String productName;
  final String fianza;
  final String price;
  final bool isOnline;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Hero(
            tag: "${productPath}",
            child: Image.network(
              "$productPath",
              width: 120.0,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("$category"),
              Text(
                "$productName",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "Fianza",
                style: TextStyle(
                  color: AppColors.kPrimary,
                ),
              ),
              Text(
                isOnline ? "$fianza" : "Segun presupuesto",
              ),
              Text(isOnline ? "$price" : ""),
            ],
          ),
        ),
      ],
    );
  }
}
