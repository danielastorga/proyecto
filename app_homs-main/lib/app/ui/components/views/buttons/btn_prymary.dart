import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';


class BtnPrimary extends StatelessWidget {
  BtnPrimary({
    this.text,
    this.onPressed,
  });

  final void Function()? onPressed;
  final String? text;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: AppColors.kPrimary,
        //gradient: AppTheme.kGradientPrimary,
      ),
      child: MaterialButton(
        minWidth: double.infinity,
        height: 60.0,
        onPressed: onPressed,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 10.0,
          ),
          child: Text(
            // "$text",
              "$text",
            style: Theme.of(context)
                .textTheme
                .subtitle1
                ?.copyWith(color: Colors.white, fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }
}
