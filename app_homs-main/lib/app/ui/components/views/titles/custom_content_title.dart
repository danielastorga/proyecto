
import 'package:app_homs_rentals/core/theme/app_text_theme.dart';
import 'package:flutter/material.dart';

class CustomContentTitle extends StatelessWidget {
  const CustomContentTitle({
    Key? key,
    required this.title,
    this.hmargin = 15.0,
    this.vmargin = 15.0,
  }) : super(key: key);

  final String title;
  final double hmargin;
  final double vmargin;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: hmargin,
        vertical: vmargin,
      ),
      child: Text(
        title,
        style: AppTextTheme(context).titleContent(),
      ),
    );
  }
}
