import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/theme/app_text_theme.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomSearchProduct extends StatelessWidget {
  const CustomSearchProduct({
    Key? key,
    this.onTap,
  }) : super(key: key);

  final void Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 50.0,
        margin: const EdgeInsets.symmetric(
          vertical: 20.0,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          color: AppColors.kDefault,
          borderRadius: BorderRadius.circular(
            AppValues.kRadiusMedium,
          ),
        ),
        child: Row(
          children: [
            SvgPicture.asset(
              "assets/icons/search.svg",
              color: AppColors.kDisabled,
              width: 22.0,
            ),
            const SizedBox(width: 10.0),
            Text(
              "¿Qué alquilaremos hoy?",
              style: AppTextTheme(context).bodyMediumRegular(),
            ),
          ],
        ),
      ),
    );
  }
}
