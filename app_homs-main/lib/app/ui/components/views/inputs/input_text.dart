import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class InputText extends StatelessWidget {
  InputText({
    this.inputFormatter,
    this.isMoney = false,
    this.isOTP = false,
    this.textAlign = TextAlign.start,
    this.textInputAction = TextInputAction.done,
    this.textCapitalization,
    this.autofocus = false,
    this.initialValue,
    this.readOnly = false,
    this.controller,
    this.iconPrefix,
    this.iconColor,
    this.labelText,
    this.filled = true,
    this.fontColor,
    this.fontSize,
    this.enabledBorderColor,
    this.focusedBorderColor,
    this.validator,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.suffixIcon,
    this.hintText,
    this.minLines = 1,
    this.maxLines,
    this.maxLength = 70,
    this.border,
    this.onChanged,
    this.onEditingComplete,
    this.onTap,
    this.onSaved,
  });

  final List<TextInputFormatter>? inputFormatter;
  final bool isMoney;
  final bool isOTP;
  final TextAlign textAlign;
  final TextInputAction textInputAction;
  final TextCapitalization? textCapitalization;
  final bool autofocus;
  final String? initialValue;
  final bool readOnly;
  final TextEditingController? controller;
  final IconData? iconPrefix;
  final Color? iconColor;
  final String? labelText;
  final bool? filled;
  final Color? fontColor;
  final double? fontSize;
  final Color? enabledBorderColor;
  final Color? focusedBorderColor;
  final String Function(String?)? validator;
  final TextInputType? keyboardType;
  final bool obscureText;
  final Widget? suffixIcon;
  final String? hintText;
  final int? minLines;
  final int? maxLines;
  final int? maxLength;
  final InputBorder? border;
  final void Function(String)? onChanged;
  final void Function()? onEditingComplete;
  final VoidCallback? onTap;
  final void Function(String?)? onSaved;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      inputFormatters: inputFormatter,
      textAlign: textAlign,
      textInputAction: textInputAction,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      autofocus: autofocus,
      initialValue: initialValue,
      style: Theme.of(context)
          .textTheme
          .caption
          ?.copyWith(color: fontColor, fontWeight: FontWeight.bold),
      cursorColor: AppColors.kPrimary,
      decoration: InputDecoration(
        border: border,
        icon: iconPrefix != null
            ? Icon(
                iconPrefix,
                color: iconColor,
                size: 18.0,
              )
            : null,
        labelText: labelText,
        labelStyle: Theme.of(context)
            .textTheme
            .caption
            ?.copyWith(color:AppColors.kSubLetter),
        filled: filled,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: .4,
            color: enabledBorderColor ?? Colors.white,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: focusedBorderColor ?? Colors.white,
          ),
        ),
        suffixIcon: suffixIcon,
        hintText: hintText,
        hintStyle: isMoney
            ? Theme.of(context).textTheme.headline3?.copyWith(
                color: fontColor!.withOpacity(0.3), fontWeight: FontWeight.bold)
            : null,
        counterText: "",
      ),
      readOnly: readOnly,
      onEditingComplete: onEditingComplete,
      controller: controller,
      onSaved: onSaved,
      onChanged: onChanged,
      onTap: onTap,
      keyboardType: keyboardType,
      obscureText: obscureText,
      maxLines: maxLines,
      minLines: minLines,
      maxLength: maxLength,
      validator: validator,
    );
  }
}
