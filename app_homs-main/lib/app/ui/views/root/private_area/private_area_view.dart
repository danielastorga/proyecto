import 'dart:ui';

import 'package:app_homs_rentals/app/ui/views/root/private_area/private_area_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PrivateAreaView extends StatelessWidget {
  const PrivateAreaView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PrivateAreaController>(
      builder: (controller) => Scaffold(
        appBar: AppBar(),
        body: SafeArea(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
            child: Column(
              children: [
                //Icon(Icons.arrow_back_ios),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.0),
                  width: double.infinity,
                  height: 500.0,
                  child: ListView(
                    children: [
                      RichText(
                          text: const TextSpan(
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                              ),
                              children: [
                            TextSpan(
                                text: "Hola, ",
                                style: TextStyle(color: Colors.black)),
                            TextSpan(
                              text: "ACSA OBRAS E INFRAESTRUCTURAS, S.A.U",
                              style: TextStyle(
                                color: AppColors.kPrimary,
                              ),
                            ),
                          ])),
                      SizedBox(
                        height: 20.0,
                      ),
                      RichText(
                          text: const TextSpan(
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.normal,
                                fontSize: 14.0,
                              ),
                              children: [
                            TextSpan(
                                text: "Esta es tu ",
                                style: TextStyle(color: Colors.black)),
                            TextSpan(
                              text: "Área Privada en Germans Homs",
                              style: TextStyle(
                                color: AppColors.kPrimary,
                              ),
                            ),
                            TextSpan(
                                text:
                                    " desde la cual puedes gestionar tus datos en Mi Perfil y consultar tus pedidos, albaranes, facturas y descargar documentación en Mis Alquileres.",
                                style: TextStyle(color: Colors.black)),
                          ])),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        width: double.infinity,
                        height: 100.0,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(217, 217, 217, 1.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.all(10.0),
                              child: Image.asset("assets/images/Captura_mi_perfil.JPG"),
                            ),
                            Text("MI PERFIL",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                )),
                          ],
                        ),
                      ),

                       SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        width: double.infinity,
                        height: 100.0,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(217, 217, 217, 1.0),
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                           Container(
                             padding: EdgeInsets.only(top: 10.0, right: 22.0, bottom: 10.0, left: 30.0),
                            child:  Image.asset("assets/images/Captura_miperfil_2.JPG"),
                           ),
                            Text("MIS ALQUILERES",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
