import 'package:app_homs_rentals/app/ui/views/root/private_area/private_area_controller.dart';
import 'package:get/get.dart';

class PrivateAreaBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
     Get.lazyPut(() => PrivateAreaController());
  }

}