import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/components/views/product/custom_card_product.dart';
import 'package:app_homs_rentals/app/ui/views/root/product/product_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductView extends StatelessWidget {
  const ProductView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductController>(
      init: ProductController(),
      builder: (_) => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Container(
            height: 50.0,
            margin: const EdgeInsets.symmetric(
              vertical: 20.0,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
              color: AppColors.kDefault,
              borderRadius: BorderRadius.circular(
                AppValues.kRadiusMedium,
              ),
            ),
            child: Row(
              children: [
                Text("${_.categories.name}", style: TextStyle(
                  color: AppColors.kLetter,
                  fontSize: 14.0,
                  )),
                Spacer(),
                GestureDetector(
                  onTap: () => Get.back(),
                  child: CircleAvatar(
                    radius: 12.5,
                    backgroundColor: AppColors.kDisabled,
                    child: Icon(Icons.close, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Obx(
          () => GridView.builder(
            primary: false,
            //padding: const EdgeInsets.all(20),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 2,
              mainAxisExtent: 260.0,
            ),
            itemCount: _.products.length,
            itemBuilder: (__, index) {
              final product = _.products[index];
              String fianza = "";
              for (MetaDatum item in product.metaData ?? []) {
                if (item.key == "homs_fianza") {
                  fianza = item.value;
                  break;
                }
              }
              return GestureDetector(
                onTap: () {
                  Get.toNamed(
                    AppRoutes.VIEW_DETAIL_PRODUCT,
                    arguments: product,
                  );
                },
                child: CustomCardProduct(
                  productPath: "${product.images![0].src}",
                  category: "${product.categories![0].name}",
                  productName: "${product.name}",
                  fianza: "$fianza €",
                  price: "${product.price} € / día",
                  isOnline: true,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
