import 'package:app_homs_rentals/app/data/models/response/response_category_model.dart';
import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/data/repositories/product_repository.dart';
import 'package:get/get.dart';

class ProductController extends GetxController {
  @override
  void onInit() {
    categories = Get.arguments as ResponseCategoryModel;
    super.onInit();
  }

  @override
  void onReady() {
    loadProductsByCategory();
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  //Instance
  final _productRepository = Get.find<ProductRepository>();

  ResponseCategoryModel categories = ResponseCategoryModel();

  //Variable
  RxList<ResponseProductByCategoryModel> products = RxList([]);
  //Function
  loadProductsByCategory() async {
    try {
      products.value = await _productRepository.getAllProductsByCategory(
        page: 1,
        categoryId: categories.id ?? 0,
      );

      print(products.length);
    } catch (e) {}
  }
}
