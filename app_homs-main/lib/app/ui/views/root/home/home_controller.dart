import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/data/repositories/product_repository.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    _loadProduct();
    super.onInit();
  }
  
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

   final _productRepository = Get.find<ProductRepository>();
  RxList<ResponseProductByCategoryModel> featuredProducts = RxList<ResponseProductByCategoryModel>([]);


  //Methods

  _loadProduct() async {
    try {
      featuredProducts.value = await _productRepository.getFeaturedProducts();
    } catch (e) {
      Get.snackbar(
        "error: ",
        e.toString(),
        duration: Duration(seconds: 6),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
    // update(
    //   ["tag-productos-destacados"],
    // );
  }
}