import 'package:app_homs_rentals/app/ui/components/views/titles/custom_content_title.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_controller.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/widgets/featured_products_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/widgets/header.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/widgets/quick_accesses.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (_) => Scaffold(
        backgroundColor: AppColors.kBackground,
        body: CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            const Header(),
            const SliverToBoxAdapter(
              child: QuickAccesses(),
            ),
            const SliverToBoxAdapter(
              child: CustomContentTitle(
                title: "Productos destacados",
                vmargin: 10.0,
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  FeaturedProductsWidget(),
                  CustomContentTitle(
                    title: "Centro de ayuda",
                    vmargin: 4.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 15.0),
                    child: Container(
                      width: double.infinity,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(248, 245, 254, 1.0),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CircleAvatar(
                            radius: 30.0,
                            child: Icon(Icons.live_help,
                                color: AppColors.kDisabled, size: 40.0),
                            backgroundColor: Colors.white,
                          ),
                          Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "¿En qué te podemos ayudar?",
                                  style: TextStyle(color: AppColors.kPurple, fontWeight: FontWeight.bold, fontSize: 14.0,),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text("Resuelve tus dudas o coctáctanos"),
                              ]),
                        ],
                      ),
                    ),
                  ),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                  // TextField(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
