import 'package:app_homs_rentals/app/ui/views/root/home/widgets/my_rentals.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';

class QuickAccesses extends StatelessWidget {
  const QuickAccesses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      width: double.infinity,
      //height: 280.0,
      margin: EdgeInsets.symmetric(horizontal:15.0, vertical:0.0),
      decoration: BoxDecoration(
        color: Color.fromRGBO(244, 250, 252, 1.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Mejora tu experiencia",
              style: TextStyle(
                color: AppColors.kPurple,
                fontWeight: FontWeight.bold,
              )),
          SizedBox(
            height: 15.0,
          ),
          Text(
              "Deseamos ayudarte con estos accesos rápidos para mejorar tu experencia."),
          MyRentalsWidget(),
        ],
      ),
    );
  }
}
