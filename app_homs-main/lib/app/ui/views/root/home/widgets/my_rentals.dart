import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyRentalsWidget extends StatelessWidget {
  const MyRentalsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _widthDevice = MediaQuery.of(context).size.width;
    return GetBuilder<HomeController>(
      builder: (controller) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 0.0),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal:0, vertical: 15.0),
            width: double.infinity,
            //height: (_widthDevice / 2) - 55,
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      //padding: EdgeInsets.all(22.0),
                      child: Icon(
                        Icons.bar_chart,
                        color: Colors.white,
                        size: 50.0,
                      ),
                      // child: Image.asset(
                      //   "assets/images/mis_alquileres.png",
                      //     fit: BoxFit.cover),
                      // width: (_widthDevice / 2) - 45,
                      // height: (_widthDevice / 2) - (45 * 2),
                      width: 60.0,
                      height: 60.0,
                      decoration: BoxDecoration(
                        color: AppColors.kPrimary,
                        // borderRadius: BorderRadius.circular(15.0),
                        // border: Border.all(
                        //   color: AppTheme.korangeLight,
                        //   width: 5.0,
                        // ),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    GestureDetector(
                      onTap: (() {
                           Helpers.goNextView(widget: AppRoutes.VIEW_LOGIN);
                      }),
                      child: Container(
                         alignment: Alignment.center,
                        width: 150.0,
                        height: 35.0,
                        padding:
                            EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColors.kPurple),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Text("Ver mis alquileres",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: AppColors.kPurple,
                            )),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      // padding: EdgeInsets.all(18.0),
                      child: Icon(Icons.app_registration,
                          size: 50.0, color: Colors.white),
                      // width: (_widthDevice / 2) - 45,
                      // height: (_widthDevice / 2) - (45 * 2),
                      width: 60.0,
                      height: 60.0,
                      decoration: BoxDecoration(
                        color: AppColors.kPrimary,
                        // borderRadius: BorderRadius.circular(15.0),
                        border: Border.all(
                          color: AppColors.kPrimary,
                          width: 5.0,
                        ),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    GestureDetector(
                      onTap: (){
                        //Helpers.goNextView(widget: AppRoutes.VIEW_LOGIN);
                      },
                      child: Container(
                        alignment: Alignment.center,
                         width: 150.0,
                        height: 35.0,
                        padding:
                            EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: AppColors.kPurple),
                          borderRadius: BorderRadius.circular(10.0),
                          color: AppColors.kPurple,
                        ),
                        child: Text("Regístrate",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: AppColors.kBackground,
                            )),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            // child: Text("${_widthDevice}"),
          ),
        );
      },
    );
  }
}
