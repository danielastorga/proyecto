import 'dart:math';

import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/components/views/search/custom_search_product.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/theme/app_text_theme.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => SliverAppBar(
        //stretch: true,
        pinned: true,
        snap: false,
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(90.0),
          child: Container(
            margin: const EdgeInsets.symmetric(
              horizontal: AppValues.kMarginApp,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _appBar(context),
                CustomSearchProduct(
                  onTap: () {
                    Get.toNamed(AppRoutes.VIEW_SEARCH);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _appBar(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset("assets/icons/homs.svg"),
        const Spacer(),
        CircleAvatar(
          radius: 16.0,
          backgroundColor: AppColors.kDefault,
          child: SvgPicture.asset(
            "assets/icons/notifications.svg",
            color: AppColors.kSecondary,
          ),
        ),
        Container(
          height: 35.0,
          padding: const EdgeInsets.only(
            left: 5.0,
            right: 10.0,
          ),
          margin: const EdgeInsets.only(left: 10.0),
          decoration: BoxDecoration(
            color: AppColors.kPrimary,
            borderRadius: BorderRadius.circular(
              AppValues.kRadiusLarge,
            ),
          ),
          child: Row(
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: CircleAvatar(
                      radius: 14.0,
                      backgroundColor: Colors.white30,
                      child: SvgPicture.asset(
                        "assets/icons/shopping.svg",
                        width: 18.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 2,
                    right: -8,
                    child: CircleAvatar(
                      backgroundColor: AppColors.kPurple,
                      radius: 8.0,
                      child: Text(
                        "9",
                        style: AppTextTheme(context).captionBold(),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(width: 10.0),
              Text(
                "€ 0.00",
                style: AppTextTheme(context).bodySmallBold(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
