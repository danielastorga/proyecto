import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/components/views/product/custom_card_product_featured.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FeaturedProductsWidget extends StatelessWidget {
  const FeaturedProductsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      // init: FeaturedController2(),
      builder: (controller) => SizedBox(
        width: double.infinity,
        height: 330.0,
        child: Obx(
          () => ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: controller.featuredProducts.length,
            itemBuilder: (context, index) {
              final product = controller.featuredProducts[index];              
              String fianza = "";
              for (MetaDatum item in product.metaData ?? []) {
                if (item.key == "homs_fianza") {
                  fianza = item.value;
                  break;
                }
              }
              return GestureDetector(
                onTap: (){
                    Get.toNamed(
                    AppRoutes.VIEW_DETAIL_PRODUCT,
                    arguments: product,
                  );
                },
                child: CustomCardProductFeatured(
                  productPath: "${product.images![0].src}",
                  category: "${product.categories![0].name}",
                  productName: "${product.name}",
                   fianza: "${fianza}",
                  //fianza: "$fianza €",
                  price: "${product.price} € / día",
                  isOnline: true,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
