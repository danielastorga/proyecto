import 'package:app_homs_rentals/app/ui/views/root/root_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class RootView extends StatelessWidget {
  const RootView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RootController>(
      init: RootController(),
      builder: (_) => Obx(
        () => Scaffold(
          body: SafeArea(
            top: false,
            bottom: true,
            child: _.navigationPages![_.currentViewIndex.value],
          ),
          bottomNavigationBar: _.isLoadingMenu.value
              ? const SizedBox()
              : BottomNavigationBar(
                  onTap: _.changeCurrentViewIndex,
                  currentIndex: _.currentViewIndex.value,
                  items: List<BottomNavigationBarItem>.generate(
                    _.appNavigationMenu.length,
                    (index) => BottomNavigationBarItem(
                      icon: _iconNavigation(
                        path: "${_.appNavigationMenu[index]!["path"]}",
                        isActive: false,
                      ),
                      label: _.appNavigationMenu[index]!["menu"] ?? "",
                      activeIcon: _iconNavigation(
                        path: "${_.appNavigationMenu[index]!["path"]}",
                        isActive: true,
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Widget _iconNavigation({
    required String path,
    required bool isActive,
  }) {
    return Column(
      children: [
        SvgPicture.asset(
          path,
          color: isActive ? AppColors.kPrimary : AppColors.kDisabled,
        ),
        const SizedBox(height: 5.0),
      ],
    );
  }
}
