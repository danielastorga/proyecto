
import 'package:app_homs_rentals/app/ui/views/root/root_controller.dart';
import 'package:get/get.dart';

class RootBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() => RootController());
  }

}