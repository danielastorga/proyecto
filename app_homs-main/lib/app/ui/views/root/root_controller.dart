import 'dart:developer';
import 'package:app_homs_rentals/app/ui/views/login/login_view.dart';
import 'package:app_homs_rentals/app/ui/views/register/register_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/product/product_view.dart';
import 'package:app_homs_rentals/core/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RootController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    log('onInit() - RootController');
    _prepareViewPages();
    _appNavigationMenu();
  }

  @override
  void onReady() {
    super.onReady();
    log('onReady() - RootController');
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  //Instances

  //Variables
  RxBool isLoadingMenu = RxBool(false);
  RxList<dynamic> appNavigationMenu = RxList([]);
  RxInt currentViewIndex = 0.obs;
  List<Widget>? _navigationPages;
  List<Widget>? get navigationPages => _navigationPages;

  //Functions
  Future _appNavigationMenu() async {
    isLoadingMenu.value = true;
    appNavigationMenu.value = await Helpers.loadJsonAssets(
      "assets/json/app_navigation_menu.json",
    );
    isLoadingMenu.value = false;
  }

  void _prepareViewPages() {
    _navigationPages = const [
      HomeView(),
      HomeView(),
      LoginView(),
      HomeView(),
    ];
  }

  void changeCurrentViewIndex(int index) {
    currentViewIndex.value = index;
  }
}
