import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/data/models/section_model.dart';
import 'package:app_homs_rentals/app/data/repositories/product_repository.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/panel_complements.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/panel_descripcion.dart';
import 'package:app_homs_rentals/core/config/constant_meta_data.dart';
import 'package:app_homs_rentals/core/utils/helpers.dart';
import 'package:get/get.dart';

class ProductDetailController extends GetxController {
  @override
  void onInit() {
    initialize();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  //Instance
  final _productRepository = Get.find<ProductRepository>();
  ResponseProductByCategoryModel getArgumentsProduct =
      ResponseProductByCategoryModel();

  //List<SectionModel> sections = [];
  RxList<SectionModel> sections = RxList([]);

  //Variables
  RxList<Image> productImages = RxList([]);
  RxString currentPathImage = RxString("");
  String homsSubTitle = "";
  RxList<ResponseProductByCategoryModel> mandatoryProducts = RxList([]);
  RxList<ResponseProductByCategoryModel> optionalProducts = RxList([]);
  RxList<ResponseProductByCategoryModel> sellProducts = RxList([]);
  RxList<Attribute> attributes = RxList([]);
  RxBool showPanelComplements = RxBool(false);
  RxBool showProductsComplements = RxBool(false);

  RxBool showDescription = RxBool(false);
  RxBool showTechnicalData = RxBool(false);

  //Functions

  void initialize() {
    getArgumentsProduct = Get.arguments as ResponseProductByCategoryModel;
    productImages.value = getArgumentsProduct.images ?? [];
    currentPathImage.value = productImages[0].src ?? "";
    homsSubTitle = Helpers.getMetaDataByKey(
      items: getArgumentsProduct.metaData ?? [],
      key: ConstantMETADATA.HOMS_SUBTITULO,
    ).value;

    loadExpansionPanel();
  }

  setCurrentPathImage(String path) {
    currentPathImage.value = path;
  }

  loadExpansionPanel() async {
    //_generateSection();
    attributes.value = getArgumentsProduct.attributes ?? [];

    _mandatoryComplements();
    _opcionalComplements();
    _sellComplements();
  }

  /* _generateSection() {
    sections.add(
      SectionModel(
        title: "Descripción",
        body: const PanelDescription(),
      ),
    );
    sections.add(
      SectionModel(
        title: "Datos técnicos",
        body: const PanelComplements(),
      ),
    );
  } */

  _mandatoryComplements() async {
    final mandatoryComplements = getArgumentsProduct.metaData
        ?.where(
            (item) => item.key == ConstantMETADATA.HOMS_COMPLEMENTO_OBLIGATORIO)
        .toList();

    if (mandatoryComplements!.isNotEmpty) {
      String include = "";
      for (MetaDatum item in mandatoryComplements) {
        include += item.value.toString() + ",";
      }

      mandatoryProducts.value =
          await _productRepository.getAllProductComplements(
        page: 1,
        include: include,
      );
      showPanelComplements.value = true;
    }
  }

  _opcionalComplements() async {
    final optionalComplements = getArgumentsProduct.metaData
        ?.where(
            (item) => item.key == ConstantMETADATA.HOMS_COMPLEMENTO_OPCIONAL)
        .toList();

    if (optionalComplements!.isNotEmpty) {
      String include = "";
      for (MetaDatum item in optionalComplements) {
        include += item.value.toString() + ",";
      }

      optionalProducts.value =
          await _productRepository.getAllProductComplements(
        page: 1,
        include: include,
      );
      showPanelComplements.value = true;
    }
  }

  _sellComplements() async {
    final sellComplements = getArgumentsProduct.metaData
        ?.where((item) => item.key == ConstantMETADATA.HOMS_COMPLEMENTO_VENTA)
        .toList();

    if (sellComplements!.isNotEmpty) {
      String include = "";
      for (MetaDatum item in sellComplements) {
        include += item.value.toString() + ",";
      }

      sellProducts.value = await _productRepository.getAllProductComplements(
        page: 1,
        include: include,
      );
      showPanelComplements.value = true;
    }
  }
}

