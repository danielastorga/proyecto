import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/ui/components/views/product/custom_card_product_complement.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MandatoryProducts extends StatelessWidget {
  const MandatoryProducts({
    Key? key,
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Obx(
        () => GridView.builder(
          primary: false,
          //padding: const EdgeInsets.all(20),
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            mainAxisExtent: 220.0,
          ),
          itemCount: _.mandatoryProducts.length,
          itemBuilder: (__, index) {
            final product = _.mandatoryProducts[index];
            return CustomCardProductComplement(
              productPath: product.images?[0].src ?? "",
              productName: "${product.name}",
              price: "${product.price}",
            );
          },
        ),
      ),
    );
  }
}

