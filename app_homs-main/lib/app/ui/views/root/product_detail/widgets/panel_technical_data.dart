import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PanelTechnicalData extends StatelessWidget {
  const PanelTechnicalData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Container(
        margin: const EdgeInsets.symmetric(
          horizontal: AppValues.kMarginApp,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              onTap: () {
                _.showTechnicalData.value = !_.showTechnicalData.value;
              },
              contentPadding: const EdgeInsets.all(0.0),
              title: const Text(
                "Datos técnicos",
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: Obx(() => _.showTechnicalData.value
                  ? const Icon(Icons.keyboard_arrow_up_outlined)
                  : const Icon(Icons.keyboard_arrow_down_outlined)),
            ),
            Obx(
              () => _.showTechnicalData.value
                  ? Container(
                      margin: const EdgeInsets.only(top: 10.0, bottom: 40.0),
                      child: MediaQuery.removePadding(
                        removeTop: true,
                        context: context,
                        child: ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (__, index) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 220.0,
                                  child: Text("${_.attributes[index].name}"),
                                ),
                                Container(
                                  alignment: Alignment.centerRight,
                                  width: 150.0,
                                  child: Text("${_.attributes[index].options?[0]}"),),
                              ],
                            );
                          },
                          separatorBuilder: (__, ___) {
                            return const Divider();
                          },
                          itemCount: _.attributes.length,
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
          ],
        ),
      ),
    );
  }
}
