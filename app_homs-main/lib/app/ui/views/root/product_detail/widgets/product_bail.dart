import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductBail extends StatelessWidget {
  const ProductBail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (__) {
        final product = __.getArgumentsProduct;
        String fianza = "";
        for (MetaDatum item in product.metaData ?? []) {
          if (item.key == "homs_fianza") {
            fianza = item.value;
            break;
          }
        }
        return ListTile(
          title: Text("Fianza"),
          subtitle: Text(
            "${fianza} €",
            style: TextStyle(
              color: AppColors.kPrimary,
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
          trailing: Container(
            alignment: Alignment.center,
            width: 135.0,
            height: 40.0,
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            decoration: BoxDecoration(
              color: AppColors.kPrimary,
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Text(
              "Alquilar",
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      },
    );
  }
}
