import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/mandatory_products.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/optional_products.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/sell_products.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PanelComplements extends StatelessWidget {
  const PanelComplements({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Container(
        margin: const EdgeInsets.symmetric(
          horizontal: AppValues.kMarginApp,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              onTap: () {
                _.showProductsComplements.value =
                    !_.showProductsComplements.value;
              },
              contentPadding: const EdgeInsets.all(0.0),
              title: const Text(
                "Complementos",
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: Obx(() => _.showProductsComplements.value
                  ? const Icon(Icons.keyboard_arrow_up_outlined)
                  : const Icon(Icons.keyboard_arrow_down_outlined)),
            ),
            Obx(
              () => _.showProductsComplements.value
                  ? Column(
                      children: [
                        Obx(
                          () => _.mandatoryProducts.isNotEmpty
                              ? _sectionComplements(
                                  title: "Obligatorios de alquiler",
                                  child: const MandatoryProducts(),
                                )
                              : const SizedBox(),
                        ),
                        Obx(
                          () => _.optionalProducts.isNotEmpty
                              ? _sectionComplements(
                                  title: "Opcionales de alquiler",
                                  child: const OptionalProducts(),
                                )
                              : const SizedBox(),
                        ),
                        Obx(
                          () => _.sellProducts.isNotEmpty
                              ? _sectionComplements(
                                  title: "Opcionales de Venta",
                                  child: const SellProducts(),
                                )
                              : const SizedBox(),
                        ),
                      ],
                    )
                  : const SizedBox(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _sectionComplements({
    required String title,
    required Widget child,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        child,
      ],
    );
  }
}

