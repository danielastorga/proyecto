import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ExpansionPanelDetail extends StatelessWidget {
  const ExpansionPanelDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => ExpansionPanelList(
        //expansionCallback: (int index, bool isExpanded) {},
        children: _.sections.map((item) {
          return ExpansionPanel(
            headerBuilder: (BuildContext context, bool isExpanded) {
              return ListTile(
                title: Text(item.title),
              );
            },
            isExpanded: true, //item.isExpanded,
            body: item.body,
          );
        }).toList(),
      ),
    );
  }
}
