import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';

class PanelDescription extends StatelessWidget {
  const PanelDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) {
        return Container(
          margin: const EdgeInsets.symmetric(
            horizontal: AppValues.kMarginApp,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                onTap: () {
                  _.showDescription.value = !_.showDescription.value;
                },
                contentPadding: const EdgeInsets.all(0.0),
                title: const Text(
                  "Descripción",
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                trailing: Obx(() => _.showDescription.value
                  ? const Icon(Icons.keyboard_arrow_up_outlined)
                  : const Icon(Icons.keyboard_arrow_down_outlined)),
              ),
              Obx(
                () => _.showDescription.value
                    ? HtmlWidget("${_.getArgumentsProduct.description}")
                    : const SizedBox(),
              ),
            ],
          ),
        );
      },
    );
  }
}

