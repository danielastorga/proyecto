import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HeaderImages extends StatelessWidget {
  const HeaderImages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) {
        return Column(
          children: [
            Obx(
              () => Hero(
                tag:  "${_.currentPathImage}",
                child: Image.network(
                  "${_.currentPathImage}",
                  width: 320.0,
                ),
              ),
            ),
            Images(),
          ],
        );
      },
    );
  }
}

class Images extends StatelessWidget {
  const Images({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Container(
        width: double.infinity,
        height: 60,
        color: Colors.white,
        child: Obx(
          () => ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _.productImages.length,
            itemBuilder: (__, index) {
              final product = _.productImages[index];
              return GestureDetector(
                onTap: () {
                  _.setCurrentPathImage(product.src ?? "");
                },
                child: Container(
                  width: 60.0,
                  height: 60.0,
                  margin: const EdgeInsets.only(left: 20.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AppValues.kRadiusSmall),
                    border: Border.all(
                      color: AppColors.kDisabled,
                    ),
                    image: DecorationImage(
                      image: NetworkImage(product.src ?? ""),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
