import 'package:app_homs_rentals/app/ui/components/views/product/custom_card_product_complement.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SellProducts extends StatelessWidget {
  const SellProducts({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Obx(
        () => GridView.builder(
          primary: false,
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            mainAxisExtent: 230.0,
          ),
          itemCount: _.sellProducts.length,
          itemBuilder: (__, index) {
            final product = _.sellProducts[index];
            return CustomCardProductComplement(
              productPath: product.images?[0].src ?? "",
              productName: "${product.name}",
              price: "${product.price}",
            );
          },
        ),
      ),
    );
  }
}
