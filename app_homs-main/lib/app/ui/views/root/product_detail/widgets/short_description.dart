import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/theme/app_values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';

class ShortDescription extends StatelessWidget {
  const ShortDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppValues.kMarginApp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20.0),
            ListTile(
              contentPadding: const EdgeInsets.all(0.0),
              title: Text(
                "${_.getArgumentsProduct.categories?[0].name}",
                style: TextStyle(
                  color: AppColors.kSubLetter,
                  fontSize: 12.0,
                ),
                // "${_.getArgumentsProduct.categories?[0].id} -${_.getArgumentsProduct.categories?[0].name}",
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                SizedBox(height: 10.0),
                Text(
                  "${_.getArgumentsProduct.name}",
                  style: TextStyle(
                    fontSize: 18.0,
                    color: AppColors.kSecondary,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
              trailing: Text(
                "${_.getArgumentsProduct.price} / día",
              ),
            ),
            SizedBox(height: 20.0),
            Text(_.homsSubTitle, style: TextStyle(
                    fontSize: 14.0,
                    color: AppColors.kSecondary,
                    fontWeight: FontWeight.bold,
                  ),),
            SizedBox(height: 5.0),
            HtmlWidget(
              "${_.getArgumentsProduct.shortDescription}",
            ),
          ],
        ),
      ),
    );
  }
}
