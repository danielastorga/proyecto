import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_controller.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/expansion_panel_detail.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/header_images.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/panel_complements.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/panel_descripcion.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/panel_technical_data.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/product_bail.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/widgets/short_description.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductDetailView extends StatelessWidget {
  const ProductDetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDetailController>(
      builder: (_) => Scaffold(
        body: SingleChildScrollView(
          child: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const HeaderImages(),
                const ShortDescription(),
                const PanelDescription(),
                const PanelTechnicalData(),
                 Obx(
                  () => _.showPanelComplements.value
                      ? const PanelComplements()
                      : const SizedBox(),
                ),
                //ExpansionPanelDetail(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 10.0),
          color: AppColors.kDefault,
          width: double.infinity,
          height: 80.0,
          child: ProductBail(),
        ),
      ),
    );
  }
}

