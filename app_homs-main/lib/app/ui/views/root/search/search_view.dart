
import 'package:app_homs_rentals/app/ui/views/root/search/search_controller.dart';
import 'package:app_homs_rentals/app/ui/views/root/search/widgets/categories.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class SearchView extends StatelessWidget {
  const SearchView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: TextField(
             autofocus: true,
            onChanged: _.onChangeSearch,
          ),
          // actions: const [
          //   CircleAvatar(),
          // ],
        ),
        body: const Categories(),
      ),
    );
  }
}
