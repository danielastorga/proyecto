import 'package:app_homs_rentals/app/data/models/response/response_category_model.dart';
import 'package:app_homs_rentals/app/data/repositories/product_repository.dart';
import 'package:get/get.dart';

class SearchController extends GetxController {
  @override
  void onInit() {
    super.onInit();

    debounce(
      queryStringSearch,
      (_) {
        searchProductCategory(queryStringSearch.value);
      },
      time: const Duration(milliseconds: 600),
    );
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  //Instance
  final _productRepository = Get.find<ProductRepository>();

  //Variable
  RxString queryStringSearch = RxString("");
  RxList<ResponseCategoryModel> categories = RxList<ResponseCategoryModel>();
  RxBool isLoading = RxBool(false);

  //Function
  void onChangeSearch(String value) => queryStringSearch.value = value;

  searchProductCategory(String queryString) async {
    try {
      isLoading.value = true;
      categories.value = await _productRepository.searchProductCategory(
        page: 1,
        queryString: queryString,
      );
      isLoading.value = false;
    } catch (e) {
      print("Error -> $e");
    }
  }
}
