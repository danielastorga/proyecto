import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/views/root/search/search_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Categories extends StatelessWidget {
  const Categories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      builder: (_) => Obx(
        () => _.isLoading.value
            ? Container(
                child: const CircularProgressIndicator(),
                width: double.infinity,
                alignment: Alignment.center,
                height: 50.0,
              )
            : ListView.separated(
                itemCount: _.categories.length,
                separatorBuilder: (_, __) => const Divider(),
                itemBuilder: (__, index) {
                  final category = _.categories[index];
                  return ListTile(
                    onTap: () {
                      Get.toNamed(
                        AppRoutes.VIEW_PRODUCT,
                        arguments: category,
                      );
                    },
                    title: Text("${category.name}"),
                  );
                },
              ),
      ),
    );
  }
}
