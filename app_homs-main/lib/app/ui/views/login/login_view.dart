
import 'package:app_homs_rentals/app/ui/views/login/login_controller.dart';
import 'package:app_homs_rentals/app/ui/views/login/widgets/login_form.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      init:LoginController() ,
      builder: (_) => Scaffold(
        backgroundColor: AppColors.kBackground,
        appBar: AppBar(title: Text("Login View"),elevation: 0.0, backgroundColor: AppColors.kBackground),
        body: ListView(
          children:  [
            //const LogoHeader(),
            //SizedBox(height: 100.h),
            const LoginForm(),
            // const ForgotPassword(),
            //const Footer(),
          ],
        ),
      ),
    );
  }
}
