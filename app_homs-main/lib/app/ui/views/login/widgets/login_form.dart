import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/components/views/buttons/btn_prymary.dart';
import 'package:app_homs_rentals/app/ui/components/views/inputs/input_text.dart';
import 'package:app_homs_rentals/app/ui/views/login/login_controller.dart';
import 'package:app_homs_rentals/core/theme/app_colors.dart';
import 'package:app_homs_rentals/core/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(builder: (_) {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        color: Colors.transparent,
        alignment: Alignment.center,
        child: Column(
          children: [
            InputText(
              //controller: _.textControllerEmail,
              iconPrefix: Icons.person_outline,
              iconColor: AppColors.kSubLetter,
              border: InputBorder.none,
              keyboardType: TextInputType.emailAddress,
              validator: null,
              maxLength: 50,
              labelText: "Correo electrónico",
              filled: false,
              enabledBorderColor: AppColors.kSubLetter,
              focusedBorderColor: AppColors.kSecondary,
              fontSize: 14.0,
              fontColor: AppColors.kSecondary,
            ),
            SizedBox(height: 20.0),
            InputText(
              //controller: _.textControllerPassword,
              iconPrefix: Icons.lock_outline,
              iconColor: AppColors.kSubLetter,
              border: InputBorder.none,
              keyboardType: TextInputType.text,
              obscureText: true,
              maxLength: 15,
              maxLines: 1,
              validator: null,
              labelText: "Contraseña",
              filled: false,
              enabledBorderColor: AppColors.kSubLetter,
              focusedBorderColor: AppColors.kSecondary,
              fontSize: 14.0,
              fontColor: AppColors.kPrimary,
              suffixIcon: const Icon(Icons.visibility_rounded),
            ),
            SizedBox(height: 50.0),
            BtnPrimary(
              text: "Iniciar sesión",
              // onPressed: _.doAuth,
              onPressed: (){
                 Get.offNamed(AppRoutes.VIEW_PRIVATE_AREA);
              },
            ),
          ],
        ),
      );
    });
  }
}