import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/views/register/register_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: Column(
        children: [
          SizedBox(height: 50.0,),
          GestureDetector(
            onTap: (){
              Get.toNamed(AppRoutes.VIEW_SEARCH);
            },
            child: Container(
              width: double.infinity,
              height: 50.0,
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                border: Border.all()
              ),
            ),
          ),


          TextButton(
            onPressed: () {
              controller.userRegister();
            },
            child: Text("Click"),
          ),
          TextButton(
            onPressed: () {
              controller.searchProductCategory();
            },
            child: Text("Search"),
          ),
        ],
      ),
    );
  }
}
