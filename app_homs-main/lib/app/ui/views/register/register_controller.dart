import 'package:app_homs_rentals/app/data/repositories/product_repository.dart';
import 'package:app_homs_rentals/app/data/repositories/user_repository.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onReady() {
    print("onReady");
    userRegister();
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  //Instance

  final _userRepository = Get.find<UserRepository>();
  final _productRepository = Get.find<ProductRepository>();

  //Variable

  //Method
  userRegister() async {
    try {
      final response = await _userRepository.user_Register();
      print(response);
    } catch (e) {
      print("Error -> $e");
    }
  }

  searchProductCategory() async {
    try {
      final response = await _productRepository.searchProductCategory(
        page: 1,
        queryString: "Plata",
      );
      print(response.length);
    } catch (e) {
      print("Error -> $e");
    }
  }
}
