import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/ui/views/login/login_binding.dart';
import 'package:app_homs_rentals/app/ui/views/login/login_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_binding.dart';
import 'package:app_homs_rentals/app/ui/views/root/home/home_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/private_area/private_area_binding.dart';
import 'package:app_homs_rentals/app/ui/views/root/private_area/private_area_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/product/product_binding.dart';
import 'package:app_homs_rentals/app/ui/views/root/product/product_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_binding.dart';
import 'package:app_homs_rentals/app/ui/views/root/product_detail/product_detail_view.dart';
import 'package:app_homs_rentals/app/ui/views/register/register_binding.dart';
import 'package:app_homs_rentals/app/ui/views/register/register_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/root_binding.dart';
import 'package:app_homs_rentals/app/ui/views/root/root_view.dart';
import 'package:app_homs_rentals/app/ui/views/root/search/search_binding.dart';
import 'package:app_homs_rentals/app/ui/views/root/search/search_view.dart';
import 'package:get/get.dart';

class AppViews {
  static final views = [
    GetPage(
      name: AppRoutes.ROOT,
      page: () => const RootView(),
      binding: RootBinding(),
    ),
    
    GetPage(
      name: AppRoutes.VIEW_HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),

    GetPage(
      name: AppRoutes.VIEW_REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: AppRoutes.VIEW_SEARCH,
      page: () => const SearchView(),
      binding: SearchBinding(),
      transition: Transition.fadeIn,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: AppRoutes.VIEW_PRODUCT,
      page: () => const ProductView(),
      binding: ProductBinding(),
      transition: Transition.fadeIn,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: AppRoutes.VIEW_DETAIL_PRODUCT,
      page: () => const ProductDetailView(),
      binding: ProductDetailBinding(),
      transition: Transition.fadeIn,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: AppRoutes.VIEW_LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
      // transition: Transition.fadeIn,
      // transitionDuration: const Duration(milliseconds: 500),
    ),
     GetPage(
      name: AppRoutes.VIEW_PRIVATE_AREA,
      page: () => const PrivateAreaView(),
      binding: PrivateAreaBinding(),
      // transition: Transition.fadeIn,
      // transitionDuration: const Duration(milliseconds: 500),
    ),
  ];
}
