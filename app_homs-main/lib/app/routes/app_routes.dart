class AppRoutes {
  static const ROOT = "/root";
  static const VIEW_HOME = "/home";
  static const VIEW_REGISTER = "/register";
  static const VIEW_SEARCH = "/search";
  static const VIEW_PRODUCT = "/product";
  static const VIEW_DETAIL_PRODUCT = "/detailProduct";
  static const VIEW_LOGIN = "/login";
  static const VIEW_PRIVATE_AREA = "/private";
}
