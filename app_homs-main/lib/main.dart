import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:app_homs_rentals/app/routes/app_views.dart';
import 'package:app_homs_rentals/core/theme/app_theme.dart';
import 'package:app_homs_rentals/core/utils/dependency_injection.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  DependencyInjection.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Homs',
      debugShowCheckedModeBanner: false,
      theme: lightTheme(context),
      initialRoute: AppRoutes.ROOT,
      getPages: AppViews.views,
    );
  }
}
