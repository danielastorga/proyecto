class ConstantMETADATA {
  static const HOMS_SUBTITULO = "homs_subtitulo";
  static const HOMS_COMPLEMENTO_OBLIGATORIO = "homs_complemento_obligatorio";
  static const HOMS_COMPLEMENTO_OPCIONAL = "homs_complemento_opcional";
  static const HOMS_COMPLEMENTO_VENTA = "homs_complemento_venta";

}
