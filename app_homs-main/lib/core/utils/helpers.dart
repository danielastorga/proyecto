import 'dart:convert';
import 'package:app_homs_rentals/app/data/models/response/response_product_by_category_model.dart';
import 'package:app_homs_rentals/app/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:get/get.dart';

class Helpers {
  static Future<List<dynamic>> loadJsonAssets(String path) async {
    final response = await rootBundle.loadString(path);
    return json.decode(response);
  }

  static MetaDatum getMetaDataByKey({
    required List<MetaDatum> items,
    required String key,
  }) =>
      items.firstWhere((item) => item.key == key);

  static goNextView({required String widget}){
     Get.toNamed(
      AppRoutes.VIEW_LOGIN,    
    );
  }
}

enum Assets {
  fonts,
  icons,
  images,
  json,
}

extension AssetsPath on Assets {
  String get path {
    switch (this) {
      case Assets.fonts:
        return "assets/fonts/";
      case Assets.icons:
        return "assets/icons/";
      case Assets.images:
        return "assets/images/";
      case Assets.json:
        return "assets/json/";
      default:
        return "No found path";
    }
  }
}
