import 'package:app_homs_rentals/app/data/providers/product_provider.dart';
import 'package:app_homs_rentals/app/data/providers/user_provider.dart';
import 'package:app_homs_rentals/app/data/repositories/product_repository.dart';
import 'package:app_homs_rentals/app/data/repositories/user_repository.dart';
import 'package:get/get.dart';

class DependencyInjection {
  static void init() {
    //Providers
    Get.put<UserProvider>(UserProvider());
    Get.put<ProductProvider>(ProductProvider());

    //Repositories
    Get.put<UserRepository>(UserRepository());
    Get.put<ProductRepository>(ProductRepository());
  }
}
