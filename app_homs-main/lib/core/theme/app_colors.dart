import 'dart:ui' show Color;

class AppColors {
  //Light Mode
  static const Color kBackground = Color.fromRGBO(255, 255, 255, 1.0);
  static const Color kBottonNavigation = Color.fromRGBO(255, 255, 255, 1.0);
  static const Color kPrimary = Color.fromRGBO(254, 86, 0, 1.0);
  static const Color kSecondary = Color.fromRGBO(2, 3, 5, 1.0);
  static const Color kPurple = Color.fromRGBO(153, 0, 255, 1.0);
  static const Color kLetter = Color.fromRGBO(82, 82, 82, 1.0);
  static const Color kSubLetter = Color.fromRGBO(147, 147, 178, 1.0);
  static const Color kDisabled = Color.fromRGBO(147, 147, 178, 1.0);
  static const Color kDefault = Color.fromRGBO(247, 248, 253, 1.0);
}
