class AppValues {
  /// Usa [kBtnHeight] para definir la altura de los buttons
  static const kBtnHeightLarge = 50.0;

  static const kBtnHeightMedium = 40.0;

  static const kBtnHeightSmall = 30.0;

  static const kBtnWidthLarge = double.infinity;

  static const kBtnWidthMedium = 140.0;

  static const kBtnWidthSmall = 90.0;

  /// Usa [kRadius] para definir el BorderRadius de: buttons, cards, containers, etc.
  static const kRadiusLarge = 20.0;

  static const kRadiusMedium = 15.0;

  static const kRadiusSmall = 8.0;

    /// Usa [kLogoSize] para definir la altura de los buttons
  static const kLogoSizeLarge = 240.0;

  static const kLogoSizeMedium = 200.0;

  static const kLogoSizeSmall = 170.0;

  /// Usa [kMarginApp] para definir el margen izquiero y derecho de la app
  static const kMarginApp = 15.0;
}
